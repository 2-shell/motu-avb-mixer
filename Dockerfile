FROM ubuntu:24.04
LABEL authors="2-Shell"

RUN DPKG_FRONTEND=noninteractive apt-get update && apt-get install --no-install-recommends -y \
        git ca-certificates clang cmake ninja-build \
        libasound2-dev libjack-jackd2-dev \
        ladspa-sdk \
        libcurl4-openssl-dev  \
        libfreetype6-dev \
        libx11-dev libxcomposite-dev libxcursor-dev libxcursor-dev libxext-dev libxinerama-dev libxrandr-dev libxrender-dev \
        libwebkit2gtk-4.0-dev \
        libglu1-mesa-dev mesa-common-dev \
    && apt-get remove -y gcc g++ \
    && apt-get autoremove -y \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

#include "OscParameterListener.h"
#include "OscAudioParameter.h"

namespace ts::mam {

OscParameterListener::OscParameterListener ( const juce::AudioProcessorValueTreeState& p,
                                             const std::map<juce::String, juce::String>& parameterVariables )
    : params ( p )
    , parameterVariableMappings ( parameterVariables )
{
    std::for_each ( parameterVariableMappings.begin (), parameterVariableMappings.end (), [this] ( const auto& pair ) {
        if ( auto param = params.getParameter ( pair.second ); param != nullptr )
            parameterVariableValues[pair.first] = String { param->getDefaultValue () };
    } );
}

void OscParameterListener::parameterChanged ( const juce::String& parameterID, float newValue )
{
    auto parameter = params.getParameter ( parameterID );

    if ( parameter == nullptr )
        return;

    for ( const auto& pair: parameterVariableMappings ) {
        if ( pair.second == parameterID ) {
            parameterVariableValues[pair.first] = String { newValue };
            return;
        }
    }

    const auto* oscParam = dynamic_cast<const OscAudioParameter*> ( parameter );
    if ( oscParam != nullptr ) {
        reconnect ();
        sender.send ( oscParam->getOscPath ( parameterVariableValues ), newValue );
    }
}


bool OscParameterListener::connect ( const juce::String& host, int port ) noexcept
{
    destinationHost = host;
    destinationPort = port;

    if ( isConnected ) {
        sender.disconnect ();
        isConnected = false;
    }

    return reconnect ();
}

bool OscParameterListener::reconnect () noexcept
{
    if ( !isConnected )
        isConnected = sender.connect ( destinationHost, destinationPort );

    return isConnected;
}

}// namespace ts::mam
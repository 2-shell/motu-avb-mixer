#include "ParameterLayoutFactoryChannel.h"
#include "ConfigFile.h"
#include "OscAudioParameter.h"

namespace ts::mam {

using juce::String;
using ParamGroupPtr = std::unique_ptr<juce::AudioProcessorParameterGroup>;

static const String chanVar {"%chan%"};

static inline String chanPath (const String& suffix) noexcept { return "mix/chan/" + chanVar + suffix; }

ParameterLayoutFactory::ParameterLayout ParameterLayoutFactoryChannel::createParameterLayout (Limits limits) noexcept
{
    return {
            makeSettingsGroup (limits), makeEQGroup(),           makeGateGroup(),    makeCompressorGroup(),
            makeAuxGroup (limits),      makeGroupGroup (limits), makeMainMixGroup(),
    };
}

const std::map<juce::String, juce::String>& ParameterLayoutFactoryChannel::getParameterVariables() const noexcept
{
    return parameterVariables;
}

ParamGroupPtr ParameterLayoutFactoryChannel::makeSettingsGroup (ts::mam::ParameterLayoutFactory::Limits limits) noexcept
{
    auto ret = makeParamGroup ("settings", juce::translate ("Settings"), "|");

    auto chanParamId = addParamId ("chan_index");
    auto chanLabel   = juce::translate ("Channel Index");

    parameterVariables[chanVar] = chanParamId;
    ret->addChild (std::make_unique<juce::AudioParameterInt> (chanParamId, chanLabel, 0, limits.channel, 0));

    return ret;
}

ParamGroupPtr ParameterLayoutFactoryChannel::makeEQGroup() noexcept
{
    return makeParamGroup (
            "eq", juce::translate ("EQ"), "|",
            makeParamGroup ("hpf", juce::translate ("HPF"), "|",
                            makeBoolOscParam (chanPath ("/hpf/enable"), addParamId ("hpf_enable"),
                                              juce::translate ("HPF Enable"), false),
                            makeFloatOscParam (chanPath ("/hpf/freq"), addParamId ("hpf_freq"),
                                               juce::translate ("HPF Frequency"),
                                               makeRangeWithSkew (20.f, 20'000.f, logSkew), 20.f)),
            makeParamGroup ("highshelf", juce::translate ("Highshelf"), "|",
                            makeBoolOscParam (chanPath ("/eq/highshelf/enable"), addParamId ("highshelf_enable"),
                                              juce::translate ("Highshelf Enable"), false),
                            makeFloatOscParam (chanPath ("/eq/highshelf/freq"), addParamId ("highshelf_freq"),
                                               juce::translate ("Highshelf Frequency"),
                                               makeRangeWithSkew (20.f, 20'000.f, logSkew), 7'000.f),
                            makeFloatOscParam (chanPath ("/eq/highshelf/gain"), addParamId ("highshelf_gain"),
                                               juce::translate ("Highshelf Gain"), -20.f, 20.f, 0.f),
                            makeFloatOscParam (chanPath ("/eq/highshelf/bw"), addParamId ("highshelf_bw"),
                                               juce::translate ("Highshelf Bandwidth"),
                                               makeRangeWithSkew (0.01f, 3.f, logSkew), 1.f),
                            makeIntOscParam (chanPath ("/eq/highshelf/mode"), addParamId ("highshelf_mode"),
                                             juce::translate ("Highshelf Mode (Shelf=0, Para=1)"), 0, 1, 0)),
            makeParamGroup ("mid1", juce::translate ("Mid 1"), "|",
                            makeBoolOscParam (chanPath ("/eq/mid1/enable"), addParamId ("mid1_enable"),
                                              juce::translate ("Mid 1 Enable"), false),
                            makeFloatOscParam (chanPath ("/eq/mid1/freq"), addParamId ("mid1_freq"),
                                               juce::translate ("Mid 1 Frequency"),
                                               makeRangeWithSkew (20.f, 20'000.f, logSkew), 5'000.f),
                            makeFloatOscParam (chanPath ("/eq/mid1/gain"), addParamId ("mid1_gain"),
                                               juce::translate ("Mid 1 Gain"), -20.f, 20.f, 0.f),
                            makeFloatOscParam (chanPath ("/eq/mid1/bw"), addParamId ("mid1_bw"),
                                               juce::translate ("Mid 1 Bandwidth"),
                                               makeRangeWithSkew (0.01f, 3.f, logSkew), 1.f)),
            makeParamGroup ("mid2", juce::translate ("Mid 2"), "|",
                            makeBoolOscParam (chanPath ("/eq/mid2/enable"), addParamId ("mid2_enable"),
                                              juce::translate ("Mid 2 Enable"), false),
                            makeFloatOscParam (chanPath ("/eq/mid2/freq"), addParamId ("mid2_freq"),
                                               juce::translate ("Mid 2 Frequency"),
                                               makeRangeWithSkew (20.f, 20'000.f, logSkew), 700.f),
                            makeFloatOscParam (chanPath ("/eq/mid2/gain"), addParamId ("mid2_gain"),
                                               juce::translate ("Mid 2 Gain"), -20.f, 20.f, 0.f),
                            makeFloatOscParam (chanPath ("/eq/mid2/bw"), addParamId ("mid2_bw"),
                                               juce::translate ("Mid 2 Bandwidth"),
                                               makeRangeWithSkew (0.01f, 3.f, logSkew), 1.f)),
            makeParamGroup ("lowshelf", juce::translate ("Lowshelf"), "|",
                            makeBoolOscParam (chanPath ("/eq/lowshelf/enable"), addParamId ("lowshelf_enable"),
                                              juce::translate ("Lowshelf Enable"), false),
                            makeFloatOscParam (chanPath ("/eq/lowshelf/freq"), addParamId ("lowshelf_freq"),
                                               juce::translate ("Lowshelf Frequency"),
                                               makeRangeWithSkew (20.f, 20'000.f, logSkew), 200.f),
                            makeFloatOscParam (chanPath ("/eq/lowshelf/gain"), addParamId ("lowshelf_gain"),
                                               juce::translate ("Lowshelf Gain"), -20.f, 20.f, 0.f),
                            makeFloatOscParam (chanPath ("/eq/lowshelf/bw"), addParamId ("lowshelf_bw"),
                                               juce::translate ("Lowshelf Bandwidth"),
                                               makeRangeWithSkew (0.01f, 3.f, logSkew), 1.f),
                            makeIntOscParam (chanPath ("/eq/lowshelf/mode"), addParamId ("lowshelf_mode"),
                                             juce::translate ("Lowshelf Mode (Shelf=0, Para=1)"), 0, 1, 0)));
}

ParamGroupPtr ParameterLayoutFactoryChannel::makeGateGroup() noexcept
{
    return makeParamGroup ("gate", juce::translate ("Gate"), "|",
                           makeBoolOscParam (chanPath ("/gate/enable"), addParamId ("gate_enable"),
                                             juce::translate ("Gate Enable"), false),
                           makeFloatOscParam (chanPath ("/gate/threshold"), addParamId ("gate_threshold"),
                                              juce::translate ("Gate Threshold"), 0.f, 1.f, 1.f),
                           makeFloatOscParam (chanPath ("/gate/attack"), addParamId ("gate_attack"),
                                              juce::translate ("Gate Attack"), 10.f, 500.f, 10.f),
                           makeFloatOscParam (chanPath ("/gate/release"), addParamId ("gate_release"),
                                              juce::translate ("Gate Release"), 50.f, 2000.f, 50.f));
}

ParamGroupPtr ParameterLayoutFactoryChannel::makeCompressorGroup() noexcept
{
    return makeParamGroup ("comp", "Comp", "|",
                           makeBoolOscParam (chanPath ("/comp/enable"), addParamId ("comp_enable"),
                                             juce::translate ("Compressor Enable"), false),
                           makeFloatOscParam (chanPath ("/comp/threshold"), addParamId ("comp_threshold"),
                                              juce::translate ("Compressor Threshold"), -40.f, 0.f, 0.f),
                           makeFloatOscParam (chanPath ("/comp/ratio"), addParamId ("comp_ratio"),
                                              juce::translate ("Compressor Ratio"), 1.f, 10.f, 1.f),
                           makeFloatOscParam (chanPath ("/comp/attack"), addParamId ("comp_attack"),
                                              juce::translate ("Compressor Attack"), 10.f, 100.f, 10.f),
                           makeFloatOscParam (chanPath ("/comp/release"), addParamId ("comp_release"),
                                              juce::translate ("Compressor Release"), 10.f, 2000.f, 10.f),
                           makeFloatOscParam (chanPath ("/comp/trim"), addParamId ("comp_trim"),
                                              juce::translate ("Compressor Trim"), -20.f, 20.f, 0.f),
                           makeIntOscParam ("mix/chan/%chan%/comp/peak", "comp_peak",
                                            juce::translate ("Compressor Mode (RMS=0, Peak=1)"), 0, 1, 1));
}

ParamGroupPtr ParameterLayoutFactoryChannel::makeAuxGroup (ParameterLayoutFactory::Limits limits) noexcept
{
    auto ret = makeParamGroup ("aux", juce::translate ("Aux"), "|");

    for (auto i = 0; i < limits.aux; ++i) {
        auto auxIdx      = String (i);
        auto paramIdSend = addParamId ("aux_" + auxIdx + "_send");
        auto paramIdPan  = addParamId ("aux_" + auxIdx + "_pan");
        auto labelSend   = juce::translate ("Aux") + " " + String (i + 1) + " " + juce::translate ("Send");
        auto labelPan    = juce::translate ("Aux") + " " + String (i + 1) + " " + juce::translate ("Pan");
        ret->addChild (makeFloatOscParam (chanPath ("/matrix/aux/" + auxIdx + "/send"), paramIdSend, labelSend,
                                          makeRangeWithSkew (0.f, 4.f, logSkew), 0.f));
        ret->addChild (
                makeFloatOscParam (chanPath ("/matrix/aux/" + auxIdx + "/pan"), paramIdPan, labelPan, -1.f, 1.f, 0.f));
    }

    return ret;
}

ParamGroupPtr ParameterLayoutFactoryChannel::makeGroupGroup (ParameterLayoutFactory::Limits limits) noexcept
{
    auto ret = makeParamGroup ("group", juce::translate ("Group"), "|");

    for (auto i = 0, j = 1; i < limits.group; i += 2, ++j) {
        auto groupIdx    = String (i);
        auto paramIdSend = addParamId ("group_" + groupIdx + "_send");
        auto paramIdPan  = addParamId ("group_" + groupIdx + "_pan");
        auto labelSend   = juce::translate ("Group") + " " + String (j) + " " + juce::translate ("Send");
        auto labelPan    = juce::translate ("Group") + " " + String (j) + " " + juce::translate ("Pan");
        ret->addChild (makeFloatOscParam (chanPath ("/matrix/group/" + groupIdx + "/send"), paramIdSend, labelSend,
                                          makeRangeWithSkew (0.f, 4.f, logSkew), 0.f));
        ret->addChild (makeFloatOscParam (chanPath ("/matrix/group/" + groupIdx + "/pan"), paramIdPan, labelPan, -1.f,
                                          1.f, 0.f));
    }

    for (auto i = 0; i < limits.reverb; ++i) {
        auto reverbIdx   = String (i);
        auto paramIdSend = addParamId ("reverb_" + reverbIdx + "_send");
        auto paramIdPan  = addParamId ("reverb_" + reverbIdx + "_pan");
        auto labelSend   = juce::translate ("Reverb") + " " + String (i + 1) + " " + juce::translate ("Send");
        auto labelPan    = juce::translate ("Reverb") + " " + String (i + 1) + " " + juce::translate ("Pan");
        ret->addChild (makeFloatOscParam (chanPath ("/matrix/reverb/" + reverbIdx + "/send"), paramIdSend, labelSend,
                                          makeRangeWithSkew (0.f, 4.f, logSkew), 0.f));
        ret->addChild (makeFloatOscParam (chanPath ("/matrix/reverb/" + reverbIdx + "/pan"), paramIdPan, labelPan, -1.f,
                                          1.f, 0.f));
    }

    return ret;
}

ParamGroupPtr ParameterLayoutFactoryChannel::makeMainMixGroup() noexcept
{
    return makeParamGroup (
            "fader", juce::translate ("Fader"), "|",
            makeBoolOscParam (chanPath ("/matrix/enable"), addParamId ("fader_enable"),
                              juce::translate ("Fader Enable"), true),
            makeBoolOscParam (chanPath ("/matrix/mute"), addParamId ("mute"), juce::translate ("Mute"), false),
            makeBoolOscParam (chanPath ("/matrix/solo"), addParamId ("solo"), juce::translate ("Solo"), false),
            makeFloatOscParam (chanPath ("/matrix/pan"), addParamId ("pan"), juce::translate ("Pan"), -1.f, 1.f, 0.f),
            makeFloatOscParam (chanPath ("/matrix/fader"), addParamId ("fader"), juce::translate ("Fader"),
                               makeRangeWithSkew (0.f, 4.f, logSkew), 1.f));
}

} // namespace ts::mam
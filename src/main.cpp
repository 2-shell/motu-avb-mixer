#include "ParameterLayoutFactorySumming.h"
#include "ParameterLayoutFactoryAuxMix.h"
#include "ParameterLayoutFactoryChannel.h"
#include "PluginProcessor.h"
#include <memory>

namespace ts::mam {

std::unique_ptr<ParameterLayoutFactory> createParameterLayoutFactory () noexcept
{
    return std::make_unique<TS_MAM_LAYOUT_FACTORY> ();
}

} // namespace ts::mam

juce::AudioProcessor* JUCE_CALLTYPE createPluginFilter ()
{
    return new ts::mam::PluginProcessor ();
}

#pragma once
#ifndef MOTU_AVB_MIXER_PARAMETER_LAYOUT_FACTORY_H
#define MOTU_AVB_MIXER_PARAMETER_LAYOUT_FACTORY_H 1

#include <juce_audio_processors/juce_audio_processors.h>
#include <map>

namespace ts::mam {

class ParameterLayoutFactory {

public:
    struct Limits {
        int channel = 1;
        int aux     = 0;
        int group   = 0;
        int reverb  = 0;
        int main    = 1;
    };

    virtual ~ParameterLayoutFactory () = default;

    using ParameterLayout = juce::AudioProcessorValueTreeState::ParameterLayout;

    [[nodiscard]] virtual ParameterLayout createParameterLayout (Limits limits) noexcept = 0;

    [[nodiscard]] virtual const std::map<juce::String, juce::String>& getParameterVariables () const noexcept = 0;

    [[nodiscard]] virtual juce::StringArray getParameterIdentifiers () const noexcept { return parameterIdentifiers; }

protected:
    inline juce::String addParamId ( const juce::String& identifier ) noexcept
    {
        parameterIdentifiers.add ( identifier );
        return identifier;
    }

    juce::StringArray parameterIdentifiers;
};

std::unique_ptr<ParameterLayoutFactory> createParameterLayoutFactory () noexcept;

template<typename... Args>
static auto makeParamGroup (Args&& ... args)
{
    return std::make_unique<juce::AudioProcessorParameterGroup>(std::forward<Args>(args)...);
}

}// namespace ts::mam

#endif// MOTU_AVB_MIXER_PARAMETER_LAYOUT_FACTORY_H

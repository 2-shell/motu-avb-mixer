#pragma once
#ifndef MOTU_AVB_MIXER_CONFIG_H
#define MOTU_AVB_MIXER_CONFIG_H 1

#include <juce_core/juce_core.h>

namespace ts::mam {

class Config {

public:
    struct DeviceConnection {
        juce::String name;
        juce::String host;
        juce::String port;
    };

    struct ChannelCounts {
        int channel = 1;
        int aux     = 0;
        int group   = 0;
        int reverb  = 0;
        int main    = 1;
    };

    explicit Config ( juce::String JSON );

    Config (const Config&) = default;
    Config& operator= (const Config&) = default;
    Config (Config&&) = default;
    Config& operator= (Config&&) = default;

    virtual ~Config () = default;

    [[nodiscard]] virtual DeviceConnection getDefaultDeviceConnection () const;

    [[nodiscard]] virtual ChannelCounts getDefaultChannelCounts () const;

    [[nodiscard]] virtual juce::String toJSON () const;

protected:
    static void parseConfigJSON ( const juce::String JSON,
                                  DeviceConnection& defaultDeviceConnection,
                                  ChannelCounts& defaultChannelCounts );

    DeviceConnection defaultDeviceConnection;
    ChannelCounts defaultChannelCounts;
};

}// namespace ts::mam

#endif//MOTU_AVB_MIXER_CONFIG_H

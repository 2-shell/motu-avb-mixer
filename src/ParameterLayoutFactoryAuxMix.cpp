#include "ParameterLayoutFactoryAuxMix.h"
#include "ConfigFile.h"
#include "OscAudioParameter.h"

namespace ts::mam {

using juce::String;
using ParamGroupPtr = std::unique_ptr<juce::AudioProcessorParameterGroup>;

static constexpr const char* const auxVar = "%aux%";

ParameterLayoutFactory::ParameterLayout ParameterLayoutFactoryAuxMix::createParameterLayout ( Limits limits ) noexcept
{
    return {
            makeSettingsGroup ( limits ),
            makeChannelGroup ( limits ),
            makeGroupGroup ( limits ),
            makeReverbGroup ( limits ),
    };
}

const std::map<juce::String, juce::String>& ParameterLayoutFactoryAuxMix::getParameterVariables () const noexcept
{
    return parameterVariables;
}

ParamGroupPtr ParameterLayoutFactoryAuxMix::makeSettingsGroup ( ts::mam::ParameterLayoutFactory::Limits limits ) noexcept
{
    auto ret = makeParamGroup ( "settings", juce::translate ( "Settings" ), "|" );

    auto auxParamId = addParamId ( "aux_index" );
    auto auxLabel   = juce::translate ( "Aux Index" );

    parameterVariables[auxVar] = auxParamId;
    ret->addChild ( std::make_unique<juce::AudioParameterInt> ( auxParamId, auxLabel, 0, limits.aux, 0 ) );

    return ret;
}

ParamGroupPtr ParameterLayoutFactoryAuxMix::makeChannelGroup ( Limits limits ) noexcept
{
    auto ret = makeParamGroup ( "channels", juce::translate ( "Channels" ), "|" );

    for ( auto i = 0; i < limits.channel; ++i ) {
        auto chIdx   = String ( i );
        auto paramId = addParamId ( "channel_" + chIdx );
        auto label   = juce::translate ( "Channel" ) + " " + String ( i + 1 );
        ret->addChild ( makeFloatOscParam ( "mix/chan/" + chIdx + "/matrix/aux/" + auxVar + "/send",
                                            paramId, label,
                                            makeRangeWithSkew ( 0.f, 4.f, logSkew ), 1.f ) );
    }

    return ret;
}

ParamGroupPtr ParameterLayoutFactoryAuxMix::makeGroupGroup ( Limits limits ) noexcept
{
    auto ret = makeParamGroup ( "group", juce::translate ( "Group" ), "|" );

    for ( auto i = 0, j = 1; i < limits.group; i += 2, ++j ) {
        auto groupIdx = String ( i );
        auto paramId  = addParamId ( "group_" + groupIdx );
        auto label    = juce::translate ( "Group" ) + " " + String ( j );
        ret->addChild ( makeFloatOscParam ( "mix/group/" + groupIdx + "/matrix/aux/" + auxVar + "/send",
                                            paramId, label,
                                            makeRangeWithSkew ( 0.f, 4.f, logSkew ), 1.f ) );
    }

    return ret;
}

ParamGroupPtr ParameterLayoutFactoryAuxMix::makeReverbGroup ( Limits limits ) noexcept
{
    auto ret = makeParamGroup ( "reverb", juce::translate ( "Reverb" ), "|" );

    for ( auto i = 0; i < limits.reverb; ++i ) {
        auto reverbIdx = String ( i );
        auto paramId   = addParamId ( "reverb_" + reverbIdx );
        auto label     = juce::translate ( "Reverb" ) + " " + String ( i + 1 );
        ret->addChild ( makeFloatOscParam ( "mix/reverb/" + reverbIdx + "/matrix/aux/" + auxVar + "/send",
                                            paramId, label,
                                            makeRangeWithSkew ( 0.f, 4.f, logSkew ), 1.f ) );
    }

    return ret;
}

}// namespace ts::mam
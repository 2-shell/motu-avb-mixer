#pragma once
#ifndef MOTU_AVB_MIXER_OSC_PARAMETER_LISTENER_H
#define MOTU_AVB_MIXER_OSC_PARAMETER_LISTENER_H 1

#include <juce_audio_utils/juce_audio_utils.h>
#include <juce_osc/juce_osc.h>

namespace ts::mam {

class OscParameterListener : public juce::AudioProcessorValueTreeState::Listener {

public:
    OscParameterListener ( const juce::AudioProcessorValueTreeState& p,
                           const std::map<juce::String, juce::String>& parameterVariables );
    ~OscParameterListener () override = default;

    void parameterChanged ( const juce::String& parameterID, float newValue ) override;

    bool connect ( const juce::String& host, int port ) noexcept;
    bool reconnect () noexcept;

protected:
    juce::String destinationHost = "127.0.0.1";
    int destinationPort          = 9998;
    bool isConnected             = false;

private:
    const juce::AudioProcessorValueTreeState& params;
    std::map<juce::String, juce::String> parameterVariableMappings;
    std::map<juce::String, juce::String> parameterVariableValues;
    juce::OSCSender sender;
};

}// namespace ts::mam

#endif//MOTU_AVB_MIXER_OSC_PARAMETER_LISTENER_H

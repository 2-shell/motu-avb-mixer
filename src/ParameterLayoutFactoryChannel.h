#pragma once
#ifndef MOTU_AVB_MIXER_PARAMETER_LAYOUT_CHANNEL_H
#define MOTU_AVB_MIXER_PARAMETER_LAYOUT_CHANNEL_H 1

#include "ParameterLayoutFactory.h"

namespace ts::mam {

class ParameterLayoutFactoryChannel : public ParameterLayoutFactory {
public:
    ParameterLayoutFactoryChannel ()           = default;
    ~ParameterLayoutFactoryChannel () override = default;

    [[nodiscard]] ParameterLayout createParameterLayout ( Limits limits ) noexcept override;

    [[nodiscard]] const std::map<juce::String, juce::String>& getParameterVariables () const noexcept override;

protected:
    std::map<juce::String, juce::String> parameterVariables;

private:
    std::unique_ptr<juce::AudioProcessorParameterGroup> makeSettingsGroup ( Limits limits ) noexcept;
    std::unique_ptr<juce::AudioProcessorParameterGroup> makeEQGroup () noexcept;
    std::unique_ptr<juce::AudioProcessorParameterGroup> makeGateGroup () noexcept;
    std::unique_ptr<juce::AudioProcessorParameterGroup> makeCompressorGroup () noexcept;
    std::unique_ptr<juce::AudioProcessorParameterGroup> makeAuxGroup ( Limits limits ) noexcept;
    std::unique_ptr<juce::AudioProcessorParameterGroup> makeGroupGroup ( Limits limits ) noexcept;
    std::unique_ptr<juce::AudioProcessorParameterGroup> makeMainMixGroup () noexcept;
};

}// namespace ts::mam

#endif// MOTU_AVB_MIXER_PARAMETER_LAYOUT_CHANNEL_H

#pragma once
#ifndef MOTU_AVB_MIXER_PARAMETER_LAYOUT_SUMMING_H
#define MOTU_AVB_MIXER_PARAMETER_LAYOUT_SUMMING_H 1

#include "ParameterLayoutFactory.h"

namespace ts::mam {

class ParameterLayoutFactorySumming : public ParameterLayoutFactory {
public:
    ParameterLayoutFactorySumming ()           = default;
    ~ParameterLayoutFactorySumming () override = default;

    [[nodiscard]] ParameterLayout createParameterLayout ( Limits limits ) noexcept override;

    [[nodiscard]] const std::map<juce::String, juce::String>& getParameterVariables () const noexcept override;

protected:
    std::map<juce::String, juce::String> parameterVariables;

private:
    std::unique_ptr<juce::AudioProcessorParameterGroup> makeChannelGroup ( Limits limits ) noexcept;
    std::unique_ptr<juce::AudioProcessorParameterGroup> makeAuxGroup ( Limits limits ) noexcept;
    std::unique_ptr<juce::AudioProcessorParameterGroup> makeGroupGroup ( Limits limits ) noexcept;
    std::unique_ptr<juce::AudioProcessorParameterGroup> makeReverbGroup ( Limits limits ) noexcept;
    std::unique_ptr<juce::AudioProcessorParameterGroup> makeMainGroup ( Limits limits ) noexcept;
};

}// namespace ts::mam

#endif// MOTU_AVB_MIXER_PARAMETER_LAYOUT_SUMMING_H

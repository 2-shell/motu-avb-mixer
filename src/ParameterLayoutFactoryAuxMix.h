#pragma once
#ifndef MOTU_AVB_MIXER_PARAMETER_LAYOUT_AUX_MIX_H
#define MOTU_AVB_MIXER_PARAMETER_LAYOUT_AUX_MIX_H 1

#include "ParameterLayoutFactory.h"

namespace ts::mam {

class ParameterLayoutFactoryAuxMix : public ParameterLayoutFactory {
public:
    ParameterLayoutFactoryAuxMix ()           = default;
    ~ParameterLayoutFactoryAuxMix () override = default;

    [[nodiscard]] ParameterLayout createParameterLayout ( Limits limits ) noexcept override;

    [[nodiscard]] const std::map<juce::String, juce::String>& getParameterVariables () const noexcept override;

protected:
    std::map<juce::String, juce::String> parameterVariables;

private:
    std::unique_ptr<juce::AudioProcessorParameterGroup> makeSettingsGroup ( Limits limits ) noexcept;
    std::unique_ptr<juce::AudioProcessorParameterGroup> makeChannelGroup ( Limits limits ) noexcept;
    std::unique_ptr<juce::AudioProcessorParameterGroup> makeGroupGroup ( Limits limits ) noexcept;
    std::unique_ptr<juce::AudioProcessorParameterGroup> makeReverbGroup ( Limits limits ) noexcept;
};

}// namespace ts::mam

#endif// MOTU_AVB_MIXER_PARAMETER_LAYOUT_AUX_MIX_H

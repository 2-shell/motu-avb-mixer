#include "Config.h"
#include <juce_core/juce_core.h>

namespace ts::mam {

using namespace juce;

static constexpr const char* const kConnections   = "connections";
static constexpr const char* const kName          = "name";
static constexpr const char* const kHost          = "host";
static constexpr const char* const kPort          = "port";
static constexpr const char* const kChannelCounts = "channelCounts";
static constexpr const char* const kChannel       = "channel";
static constexpr const char* const kAux           = "aux";
static constexpr const char* const kGroup         = "group";
static constexpr const char* const kReverb        = "reverb";
static constexpr const char* const kMain          = "main";

Config::Config ( const String JSON )
{
    parseConfigJSON ( JSON, defaultDeviceConnection, defaultChannelCounts );
}

Config::DeviceConnection Config::getDefaultDeviceConnection () const
{
    return defaultDeviceConnection;
}

Config::ChannelCounts Config::getDefaultChannelCounts () const
{
    return defaultChannelCounts;
}

static void extractDeviceConnection ( const var& config, Config::DeviceConnection& deviceConnection )
{
    deviceConnection.name = "default";
    deviceConnection.host = "127.0.0.1";
    deviceConnection.port = "9998";

    if ( !config.isObject () )
        return;

    auto nameProp = config.getProperty ( kName, "" );
    if ( nameProp.isString () ) {
        auto parsedName = nameProp.toString ();
        if ( !parsedName.isEmpty () )
            deviceConnection.name = parsedName;
    }

    auto hostProp = config.getProperty ( kHost, "" );
    if ( hostProp.isString () ) {
        auto parsedHost = hostProp.toString ();
        if ( !parsedHost.isEmpty () )
            deviceConnection.host = parsedHost;
    }

    auto portProp = config.getProperty ( kPort, "" );
    if ( portProp.isString () ) {
        auto parsedPort = portProp.toString ();
        if ( !parsedPort.isEmpty () )
            deviceConnection.port = parsedPort;
    }
}

static void extractChannelCounts ( const var& config, Config::ChannelCounts& channelCounts )
{
    channelCounts.channel = 24;
    channelCounts.aux     = 8;
    channelCounts.group   = 8;
    channelCounts.reverb  = 1;
    channelCounts.main    = 1;

    if ( !config.isObject () )
        return;

    auto channelProp = config.getProperty ( kChannel, channelCounts.channel );
    if ( channelProp.isInt () )
        channelCounts.channel = channelProp;

    auto auxProp = config.getProperty ( kAux, channelCounts.aux );
    if ( auxProp.isInt () )
        channelCounts.aux = auxProp;

    auto groupProp = config.getProperty ( kGroup, channelCounts.group );
    if ( groupProp.isInt () )
        channelCounts.group = groupProp;

    auto reverbProp = config.getProperty ( kReverb, channelCounts.reverb );
    if ( reverbProp.isInt () )
        channelCounts.reverb = reverbProp;

    auto mainProp = config.getProperty ( kMain, channelCounts.main );
    if ( mainProp.isInt () )
        channelCounts.main = mainProp;
}

void Config::parseConfigJSON ( String JSON,
                               DeviceConnection& defaultDeviceConnection,
                               ChannelCounts& defaultChannelCounts )
{
    var parsedConfig;
    Result result = JSON::parse ( JSON, parsedConfig );

    if ( result.failed () ) {
        Logger::writeToLog ( "Failed to parse config JSON: " + result.getErrorMessage () );
    }

    if ( !parsedConfig.isObject () ) {
        Logger::writeToLog ( "Config JSON is not an object" );
    }

    auto connections = parsedConfig.getProperty ( kConnections, {} );

    if ( connections.isArray () && connections.size () > 0 )
        extractDeviceConnection ( connections[0], defaultDeviceConnection );
    else
        extractDeviceConnection ( {}, defaultDeviceConnection );

    extractChannelCounts ( parsedConfig.getProperty ( kChannelCounts, {} ), defaultChannelCounts );
}

juce::String Config::toJSON () const
{
    DynamicObject::Ptr connection { new DynamicObject };
    connection->setProperty ( kName, defaultDeviceConnection.name );
    connection->setProperty ( kHost, defaultDeviceConnection.host );
    connection->setProperty ( kPort, defaultDeviceConnection.port );

    DynamicObject::Ptr counts { new DynamicObject };
    counts->setProperty ( kChannel, defaultChannelCounts.channel );
    counts->setProperty ( kAux, defaultChannelCounts.aux );
    counts->setProperty ( kGroup, defaultChannelCounts.group );
    counts->setProperty ( kReverb, defaultChannelCounts.reverb );
    counts->setProperty ( kMain, defaultChannelCounts.main );

    Array<var> connections;
    connections.add ( connection.get() );

    DynamicObject::Ptr config { new DynamicObject };
    config->setProperty ( kConnections, connections );
    config->setProperty ( kChannelCounts, counts.get() );

    return JSON::toString ( config.get() );
}

}// namespace ts::mam
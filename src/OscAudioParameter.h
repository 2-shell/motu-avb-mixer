#pragma once
#ifndef MOTU_AVB_MIX_OSC_AUDIO_PARAMETER_H
#define MOTU_AVB_MIX_OSC_AUDIO_PARAMETER_H 1

#include <juce_audio_processors/juce_audio_processors.h>

namespace ts::mam {

using juce::HashMap;
using juce::String;

constexpr float logSkew = 1.f / juce::MathConstants<float>::euler;

class OscAudioParameter {

public:
    explicit OscAudioParameter ( String oscPath )
        : _oscPath { oscPath }
    {}

    ~OscAudioParameter () = default;

    [[nodiscard]] String getOscPath () const noexcept
    {
        String ret = _oscPath;
        if ( !ret.startsWith ( "/" ) ) {
            ret = "/" + _oscPath;
        }
        return ret;
    }

    [[nodiscard]] String getOscPath ( const std::map<String, String>& variableValues ) const noexcept
    {
        auto ret { getOscPath () };
        for_each ( variableValues.begin (), variableValues.end (), [&] ( auto& pair ) {
            ret = ret.replace ( pair.first, pair.second );
        } );
        return ret;
    }

    void setOscPath ( String oscPath ) noexcept
    {
        _oscPath = oscPath;
    }

protected:
    juce::String _oscPath;
};

template<typename T>
class ConcreteOscAudioParameter : public T
    , public OscAudioParameter {

public:
    template<typename... Args>
    explicit ConcreteOscAudioParameter ( String oscPath, Args... args )
        : T { std::forward<Args> ( args )... }
        , OscAudioParameter { oscPath }
    {}

    ~ConcreteOscAudioParameter () override = default;
};

template<typename T, typename... Args>
static std::unique_ptr<T> makeOscAudioParam ( String oscPath, Args... args )
{
    return std::make_unique<ConcreteOscAudioParameter<T>> ( oscPath, std::forward<Args> ( args )... );
}

template<typename... Args>
static auto makeFloatOscParam ( Args&&... args )
{
    return makeOscAudioParam<juce::AudioParameterFloat> ( std::forward<Args> ( args )... );
}

template<typename... Args>
static auto makeBoolOscParam ( Args&&... args )
{
    return makeOscAudioParam<juce::AudioParameterBool> ( std::forward<Args> ( args )... );
}

template<typename... Args>
static auto makeIntOscParam ( Args&&... args )
{
    return makeOscAudioParam<juce::AudioParameterInt> ( std::forward<Args> ( args )... );
}

inline juce::NormalisableRange<float> makeRangeWithSkew ( float min, float max, float skew = 1.f )
{
    return juce::NormalisableRange<float> { min, max, 0.01f, skew };
}

}// namespace ts::mam

#endif// MOTU_AVB_MIX_OSC_AUDIO_PARAMETER_H

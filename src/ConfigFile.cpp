#include "ConfigFile.h"

namespace ts::mam {

using juce::File;
using juce::Result;
using juce::String;

static Result createDirectoryIfNeeded ( const File& file ) noexcept
{
    if ( !file.exists () )
        return file.createDirectory ();

    return Result::ok ();
}


Config ConfigFile::load () const noexcept
{
    auto configDirectory = getConfigDirectory ();
    auto configFile      = configDirectory.getChildFile ( getConfigFileName () );

    createDirectoryIfNeeded ( configDirectory );
    return Config { configFile.loadFileAsString () };
}

void ConfigFile::save ( const Config& config ) const noexcept
{
    auto configDirectory = getConfigDirectory ();
    auto configFile      = configDirectory.getChildFile ( getConfigFileName () );

    createDirectoryIfNeeded ( configDirectory );
    configFile.create ();
    configFile.replaceWithText ( config.toJSON () );
}

juce::File ConfigFile::getConfigDirectory () const noexcept
{
    return File::getSpecialLocation ( File::userApplicationDataDirectory )
            .getChildFile ( "de.2-shell.motu-avb-mixer" );
}

juce::String ConfigFile::getConfigFileName () const noexcept
{
    return "config.json";
}

}// namespace ts::mam
#include "ParameterLayoutFactorySumming.h"
#include "ConfigFile.h"
#include "OscAudioParameter.h"

namespace ts::mam {

using juce::String;
using ParamGroupPtr = std::unique_ptr<juce::AudioProcessorParameterGroup>;

ParameterLayoutFactory::ParameterLayout ParameterLayoutFactorySumming::createParameterLayout ( Limits limits ) noexcept
{
    return {
            makeChannelGroup ( limits ),
            makeAuxGroup ( limits ),
            makeGroupGroup ( limits ),
            makeReverbGroup ( limits ),
            makeMainGroup ( limits ),
    };
}

const std::map<juce::String, juce::String>& ParameterLayoutFactorySumming::getParameterVariables () const noexcept
{
    return parameterVariables;
}

ParamGroupPtr ParameterLayoutFactorySumming::makeChannelGroup ( Limits limits ) noexcept
{
    auto ret = makeParamGroup ( "channels", juce::translate ( "Channels" ), "|" );

    for ( auto i = 0; i < limits.channel; ++i ) {
        auto chIdx   = String ( i );
        auto paramId = addParamId ( "channel_" + chIdx );
        auto label   = juce::translate ( "Channel" ) + " " + String ( i + 1 );
        ret->addChild ( makeFloatOscParam ( "mix/chan/" + chIdx + "/matrix/fader", paramId, label,
                                            makeRangeWithSkew ( 0.f, 4.f, logSkew ), 1.f ) );
    }

    return ret;
}

ParamGroupPtr ParameterLayoutFactorySumming::makeAuxGroup ( Limits limits ) noexcept
{
    auto ret = makeParamGroup ( "aux", juce::translate ( "Aux" ), "|" );

    for ( auto i = 0; i < limits.aux; ++i ) {
        auto auxIdx  = String ( i );
        auto paramId = addParamId ( "aux_" + auxIdx );
        auto label   = juce::translate ( "Aux" ) + " " + String ( i + 1 );
        ret->addChild ( makeFloatOscParam ( "mix/aux/" + auxIdx + "/matrix/fader", paramId, label,
                                            makeRangeWithSkew ( 0.f, 4.f, logSkew ), 1.f ) );
    }

    return ret;
}

ParamGroupPtr ParameterLayoutFactorySumming::makeGroupGroup ( Limits limits ) noexcept
{
    auto ret = makeParamGroup ( "group", juce::translate ( "Group" ), "|" );

    for ( auto i = 0, j = 1; i < limits.group; i+=2, ++j ) {
        auto groupIdx = String ( i );
        auto paramId  = addParamId ( "group_" + groupIdx );
        auto label    = juce::translate ( "Group" ) + " " + String ( j );
        ret->addChild ( makeFloatOscParam ( "mix/group/" + groupIdx + "/matrix/fader", paramId, label,
                                            makeRangeWithSkew ( 0.f, 4.f, logSkew ), 1.f ) );
    }

    return ret;
}

ParamGroupPtr ParameterLayoutFactorySumming::makeReverbGroup ( Limits limits ) noexcept
{
    auto ret = makeParamGroup ( "reverb", juce::translate ( "Reverb" ), "|" );

    for ( auto i = 0; i < limits.reverb; ++i ) {
        auto reverbIdx = String ( i );
        auto paramId   = addParamId ( "reverb_" + reverbIdx );
        auto label     = juce::translate ( "Reverb" ) + " " + String ( i + 1 );
        ret->addChild ( makeFloatOscParam ( "mix/reverb/" + reverbIdx + "/matrix/fader", paramId, label,
                                            makeRangeWithSkew ( 0.f, 4.f, logSkew ), 1.f ) );
    }

    return ret;
}

ParamGroupPtr ParameterLayoutFactorySumming::makeMainGroup ( Limits limits ) noexcept
{
    auto ret = makeParamGroup ( "main", juce::translate ( "Main" ), "|" );

    for ( auto i = 0; i < limits.main; ++i ) {
        auto mainIdx = String ( i );
        auto paramId = addParamId ( "main_" + mainIdx );
        auto label   = juce::translate ( "Main" ) + " " + String ( i + 1 );
        ret->addChild ( makeFloatOscParam ( "mix/main/" + mainIdx + "/matrix/fader", paramId, label,
                                            makeRangeWithSkew ( 0.f, 4.f, logSkew ), 1.f ) );
    }

    return ret;
}

}// namespace ts::mam
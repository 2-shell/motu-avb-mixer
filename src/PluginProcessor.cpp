#include "PluginProcessor.h"
#include "Config.h"
#include "ConfigFile.h"
#include "OscParameterListener.h"

namespace ts::mam {

static Config loadConfig ()
{
    ConfigFile cfgFile;
    return cfgFile.load ();
}

static ParameterLayoutFactory::Limits getParameterLimits ( const Config& cfg )
{
    return { cfg.getDefaultChannelCounts ().channel,
             cfg.getDefaultChannelCounts ().aux,
             cfg.getDefaultChannelCounts ().group,
             cfg.getDefaultChannelCounts ().reverb,
             cfg.getDefaultChannelCounts ().main };
}

//==============================================================================
PluginProcessor::PluginProcessor ()
    : AudioProcessor ( BusesProperties ()
                               .withInput ( "Input", juce::AudioChannelSet::stereo (), true )
                               .withOutput ( "Output", juce::AudioChannelSet::stereo (), true ) )
    , config { loadConfig () }
    , parameterLayoutFactory { createParameterLayoutFactory () }
    , parameters ( *this, &parameterUndoManager, "PARAMETERS",
                   parameterLayoutFactory->createParameterLayout ( getParameterLimits ( config ) ) )
    , oscParameterListener { std::make_unique<OscParameterListener> ( parameters, parameterLayoutFactory->getParameterVariables() ) }
{
    ConfigFile cfgFile;
    cfgFile.save (config);

    auto paramIds = parameterLayoutFactory->getParameterIdentifiers ();
    std::for_each ( paramIds.begin (), paramIds.end (), [this] ( auto& name ) {
        this->parameters.addParameterListener ( name, oscParameterListener.get () );
    } );

    oscParameterListener->connect ( config.getDefaultDeviceConnection ().host, config.getDefaultDeviceConnection ().port.getIntValue () );
}

PluginProcessor::~PluginProcessor () = default;

//==============================================================================
const juce::String PluginProcessor::getName () const
{
    return JucePlugin_Name;
}

bool PluginProcessor::acceptsMidi () const
{
#if JucePlugin_WantsMidiInput
    return true;
#else
    return false;
#endif
}

bool PluginProcessor::producesMidi () const
{
#if JucePlugin_ProducesMidiOutput
    return true;
#else
    return false;
#endif
}

bool PluginProcessor::isMidiEffect () const
{
#if JucePlugin_IsMidiEffect
    return true;
#else
    return false;
#endif
}

double PluginProcessor::getTailLengthSeconds () const
{
    return 0.0;
}

int PluginProcessor::getNumPrograms ()
{
    return 1;
}

int PluginProcessor::getCurrentProgram ()
{
    return 0;
}

void PluginProcessor::setCurrentProgram ( int index )
{
    juce::ignoreUnused ( index );
}

const juce::String PluginProcessor::getProgramName ( int index )
{
    juce::ignoreUnused ( index );
    return juce::translate ( "Default" );
}

void PluginProcessor::changeProgramName ( int index, const juce::String& newName )
{
    juce::ignoreUnused ( index, newName );
}

//==============================================================================
void PluginProcessor::prepareToPlay ( double sampleRate, int samplesPerBlock )
{
    juce::ignoreUnused ( sampleRate, samplesPerBlock );
}

void PluginProcessor::releaseResources ()
{}

bool PluginProcessor::isBusesLayoutSupported ( const BusesLayout& layouts ) const
{
    if ( layouts.getMainOutputChannelSet () != juce::AudioChannelSet::mono () && layouts.getMainOutputChannelSet () != juce::AudioChannelSet::stereo () )
        return false;

    if ( layouts.getMainOutputChannelSet () != layouts.getMainInputChannelSet () )
        return false;

    return true;
}

void PluginProcessor::processBlock ( juce::AudioBuffer<float>& buffer,
                                     juce::MidiBuffer& midiMessages )
{
    juce::ignoreUnused ( midiMessages );

    juce::ScopedNoDenormals noDenormals;
    auto totalNumInputChannels  = getTotalNumInputChannels ();
    auto totalNumOutputChannels = getTotalNumOutputChannels ();

    for ( auto i = totalNumInputChannels; i < totalNumOutputChannels; ++i )
        buffer.clear ( i, 0, buffer.getNumSamples () );

    for ( int channel = 0; channel < totalNumInputChannels; ++channel )
        for ( auto j = 0; j < buffer.getNumSamples (); ++j )
            *buffer.getWritePointer ( channel, j ) =
                    *buffer.getReadPointer ( channel, j );
}

//==============================================================================
bool PluginProcessor::hasEditor () const
{
    return false;
}

juce::AudioProcessorEditor* PluginProcessor::createEditor ()
{
    return nullptr;
}

//==============================================================================
void PluginProcessor::getStateInformation ( juce::MemoryBlock& destData )
{
    if (auto currentStateXml = parameters.state.createXml())
        copyXmlToBinary(*currentStateXml, destData);
}

void PluginProcessor::setStateInformation ( const void* data, int sizeInBytes )
{
    if (auto currentStateXml = getXmlFromBinary(data, sizeInBytes))
        if (currentStateXml->hasTagName(parameters.state.getType()))
            parameters.replaceState(juce::ValueTree::fromXml(*currentStateXml));
}

}// namespace ts::mam

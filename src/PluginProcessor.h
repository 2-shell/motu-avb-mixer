#pragma once
#ifndef MOTU_AVB_MIXER_PLUGIN_PROCESSOR_H
#define MOTU_AVB_MIXER_PLUGIN_PROCESSOR_H 1

#include <juce_audio_processors/juce_audio_processors.h>
#include "Config.h"
#include "ParameterLayoutFactory.h"
#include "OscParameterListener.h"

namespace ts::mam {

class PluginProcessor : public juce::AudioProcessor
{
public:
    //==============================================================================
    PluginProcessor();
    ~PluginProcessor() override;

    //==============================================================================
    void prepareToPlay (double sampleRate, int samplesPerBlock) override;
    void releaseResources() override;

    bool isBusesLayoutSupported (const BusesLayout& layouts) const override;

    void processBlock (juce::AudioBuffer<float>&, juce::MidiBuffer&) override;
    using AudioProcessor::processBlock;

    //==============================================================================
    juce::AudioProcessorEditor* createEditor() override;
    bool hasEditor() const override;

    //==============================================================================
    const juce::String getName() const override;

    bool acceptsMidi() const override;
    bool producesMidi() const override;
    bool isMidiEffect() const override;
    double getTailLengthSeconds() const override;

    //==============================================================================
    int getNumPrograms() override;
    int getCurrentProgram() override;
    void setCurrentProgram (int index) override;
    const juce::String getProgramName (int index) override;
    void changeProgramName (int index, const juce::String& newName) override;

    //==============================================================================
    void getStateInformation (juce::MemoryBlock& destData) override;
    void setStateInformation (const void* data, int sizeInBytes) override;

    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (PluginProcessor)

protected:
    Config config;
    std::unique_ptr<ts::mam::ParameterLayoutFactory> parameterLayoutFactory;
    juce::UndoManager parameterUndoManager;
    juce::AudioProcessorValueTreeState parameters;
    std::unique_ptr<OscParameterListener> oscParameterListener;
};

}// namespace ts::mam

#endif//MOTU_AVB_MIXER_PLUGIN_PROCESSOR_H

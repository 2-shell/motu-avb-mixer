#pragma once
#ifndef MOTU_AVB_MIXER_CONFIG_FILE_H
#define MOTU_AVB_MIXER_CONFIG_FILE_H 1

#include "Config.h"
#include <juce_core/juce_core.h>
#include <string>

namespace ts::mam {

class ConfigFile {

public:
    ConfigFile () = default;
    virtual ~ConfigFile () = default;

    [[nodiscard]] Config load () const noexcept;
    void save ( const Config& config ) const noexcept;

    [[nodiscard]] virtual juce::File getConfigDirectory () const noexcept;
    [[nodiscard]] virtual juce::String getConfigFileName () const noexcept;
};

}// namespace ts::mam

#endif//MOTU_AVB_MIXER_CONFIG_FILE_H

#include <catch2/catch.hpp>
#include "ParameterLayoutFactory.h"
#include "ParameterLayoutFactorySumming.h"

std::unique_ptr<ts::mam::ParameterLayoutFactory> ts::mam::createParameterLayoutFactory () noexcept
{
    return std::make_unique<ts::mam::ParameterLayoutFactorySumming> ();
}

TEST_CASE ( "Basic ParameterLayoutFactory Tests", "[ParameterLayoutFactory]" )
{
    using namespace ts::mam;

    SECTION ( "createParameterLayoutFactory() returns a valid ParameterLayoutFactory" )
    {
        auto factory = createParameterLayoutFactory ();
        REQUIRE ( factory != nullptr );
    }

    SECTION ( "ParameterLayoutFactory::createParameterLayout() returns a valid ParameterLayout" )
    {
        auto factory = createParameterLayoutFactory ();
        REQUIRE ( factory != nullptr );
        auto layout = factory->createParameterLayout ({});
    }
}

TEST_CASE ("ParameterLayoutFactorySumming Tests", "[ParameterLayoutFactorySumming]")
{
    using namespace ts::mam;

    SECTION ("createParameterLayoutFactory() returns a valid ParameterLayoutFactory")
    {
        auto factory = std::make_unique<ParameterLayoutFactorySumming> ();
        REQUIRE (factory != nullptr);
    }

    SECTION ("ParameterLayoutFactory::createParameterLayout() returns a valid ParameterLayout")
    {
        auto factory = std::make_unique<ParameterLayoutFactorySumming> ();
        REQUIRE (factory != nullptr);
        auto layout = factory->createParameterLayout ({});
    }

    SECTION ("ParameterLayoutFactory::getParameterIdentifiers() returns the correct identifiers")
    {
        auto factory = std::make_unique<ParameterLayoutFactorySumming> ();
        REQUIRE (factory != nullptr);
        auto identifiers = factory->getParameterIdentifiers ();
        REQUIRE (identifiers.size () == 0);
        auto layout = factory->createParameterLayout ({});
        identifiers = factory->getParameterIdentifiers ();
        REQUIRE (identifiers.size () == 2);
    }

    SECTION ("ParameterLayoutFactory::getParameterVariables() returns the correct variables")
    {
        auto factory = std::make_unique<ParameterLayoutFactorySumming> ();
        REQUIRE (factory != nullptr);
        auto variables = factory->getParameterVariables ();
        REQUIRE (variables.empty());
    }
}
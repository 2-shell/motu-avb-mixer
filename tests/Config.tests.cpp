#include "Config.h"
#include <catch2/catch.hpp>

using ts::mam::Config;

Config::DeviceConnection defaultDeviceConnection { "default", "127.0.0.1", "9998" };
Config::ChannelCounts defaultChannelCounts { 24, 8, 8, 1, 1 };

inline bool operator== ( const Config::DeviceConnection& lhs, const Config::DeviceConnection& rhs )
{
    return lhs.name == rhs.name && lhs.host == rhs.host && lhs.port == rhs.port;
}

inline bool operator== ( const Config::ChannelCounts& lhs, const Config::ChannelCounts& rhs )
{
    return lhs.channel == rhs.channel && lhs.aux == rhs.aux && lhs.group == rhs.group && lhs.reverb == rhs.reverb && lhs.main == rhs.main;
}

TEST_CASE ( "Config tests", "[Config]" )
{
    using namespace ts::mam;

    SECTION ( "Empty object returns sensible defaults" )
    {
        Config cfg { "{}" };
        REQUIRE ( bool { cfg.getDefaultDeviceConnection () == defaultDeviceConnection } );
        REQUIRE ( bool { cfg.getDefaultChannelCounts () == defaultChannelCounts } );
    }

    SECTION ( "Empty string doesn't throw" )
    {
        REQUIRE_NOTHROW ( Config { "" } );
    }

    SECTION ( "Empty object returns sensible defaults" )
    {
        Config cfg { "{foo}" };
        REQUIRE ( bool { cfg.getDefaultDeviceConnection () == defaultDeviceConnection } );
        REQUIRE ( bool { cfg.getDefaultChannelCounts () == defaultChannelCounts } );
    }

    SECTION ( "Config returns sensible defaults with missing values" )
    {
        Config cfg { R"( {"connections": [], "channel": {}} )" };
        REQUIRE ( bool { cfg.getDefaultDeviceConnection () == defaultDeviceConnection } );
        REQUIRE ( bool { cfg.getDefaultChannelCounts () == defaultChannelCounts } );
    }

    SECTION ( "Config returns sensible defaults with invalid values" )
    {
        Config cfg { R"({"connections": [{"name": 1, "host": 2, "port": 3}], "channelCounts": {"channel": "foo", "aux": "bar", "group": "baz", "reverb": "qux", "main": "quux"}})" };
        REQUIRE ( bool { cfg.getDefaultDeviceConnection () == defaultDeviceConnection } );
        REQUIRE ( bool { cfg.getDefaultChannelCounts () == defaultChannelCounts } );
    }

    SECTION ( "Config returns sensible defaults with empty values" )
    {
        Config cfg { R"({"connections": [{"name": "", "host": "", "port": ""}], "channelCounts": {"channel": "", "aux": "", "group": "", "reverb": "", "main": ""}})" };
        REQUIRE ( bool { cfg.getDefaultDeviceConnection () == defaultDeviceConnection } );
        REQUIRE ( bool { cfg.getDefaultChannelCounts () == defaultChannelCounts } );
    }

    SECTION ( "Config returns correct values with valid JSON" )
    {
        Config cfg { R"({"connections": [{"name": "foo", "host": "bar", "port": "baz"}], "channelCounts": {"channel": 1, "aux": 2, "group": 3, "reverb": 4, "main": 5}})" };
        //        REQUIRE ( bool { cfg.getDefaultDeviceConnection () == Config::DeviceConnection { "foo", "bar", "baz" } } );
        REQUIRE ( cfg.getDefaultDeviceConnection ().name == "foo" );
        REQUIRE ( cfg.getDefaultDeviceConnection ().host == "bar" );
        REQUIRE ( cfg.getDefaultDeviceConnection ().port == "baz" );
        REQUIRE ( bool { cfg.getDefaultChannelCounts () == Config::ChannelCounts { 1, 2, 3, 4, 5 } } );
    }

    SECTION ("Conversion to JSON works")
    {
        auto originalJson = R"({"connections": [{"name": "foo", "host": "bar", "port": "baz"}], "channelCounts": {"channel": 1, "aux": 2, "group": 3, "reverb": 4, "main": 5}})";
        auto reformattedJson = juce::JSON::toString( juce::JSON::parse ( originalJson ));
        Config cfg { reformattedJson };
        REQUIRE ( cfg.toJSON () == reformattedJson );
    }

    SECTION ("Copy constructor works")
    {
        auto originalJson = R"({"connections": [{"name": "foo", "host": "bar", "port": "baz"}], "channelCounts": {"channel": 1, "aux": 2, "group": 3, "reverb": 4, "main": 5}})";
        auto reformattedJson = juce::JSON::toString( juce::JSON::parse ( originalJson ));
        Config cfg { reformattedJson };
        Config cfg2 { cfg };
        REQUIRE ( cfg2.toJSON () == reformattedJson );
    }

    SECTION ("Copy assignment works")
    {
        auto originalJson = R"({"connections": [{"name": "foo", "host": "bar", "port": "baz"}], "channelCounts": {"channel": 1, "aux": 2, "group": 3, "reverb": 4, "main": 5}})";
        auto reformattedJson = juce::JSON::toString( juce::JSON::parse ( originalJson ));
        Config cfg { reformattedJson };
        Config cfg2 { "" };
        cfg2 = cfg;
        REQUIRE ( cfg2.toJSON () == reformattedJson );
    }

    SECTION ("Move constructor works")
    {
        auto originalJson = R"({"connections": [{"name": "foo", "host": "bar", "port": "baz"}], "channelCounts": {"channel": 1, "aux": 2, "group": 3, "reverb": 4, "main": 5}})";
        auto reformattedJson = juce::JSON::toString( juce::JSON::parse ( originalJson ));
        Config cfg { reformattedJson };
        Config cfg2 { std::move ( cfg ) };
        REQUIRE ( cfg2.toJSON () == reformattedJson );
    }

    SECTION ("Move assignment works")
    {
        auto originalJson = R"({"connections": [{"name": "foo", "host": "bar", "port": "baz"}], "channelCounts": {"channel": 1, "aux": 2, "group": 3, "reverb": 4, "main": 5}})";
        auto reformattedJson = juce::JSON::toString( juce::JSON::parse ( originalJson ));
        Config cfg { reformattedJson };
        Config cfg2 { "" };
        cfg2 = std::move ( cfg );
        REQUIRE ( cfg2.toJSON () == reformattedJson );
    }
}

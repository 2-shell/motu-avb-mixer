#include "OscAudioParameter.h"
#include <catch2/catch.hpp>

TEST_CASE ( "Basic OscAudioParameter Tests", "[OscAudioParameter]" )
{
    using namespace ts::mam;
    using namespace std::string_literals;

    SECTION ( "OscAudioParameter::getOscPath() returns the correct path" )
    {
        OscAudioParameter param { "/foo" };
        REQUIRE ( param.getOscPath () == "/foo" );
    }

    SECTION ( "OscAudioParameter::getOscPath() returns the correct path with leading slash" )
    {
        OscAudioParameter param { "foo" };
        REQUIRE ( param.getOscPath () == "/foo" );
    }

    SECTION ( "OscAudioParameter::getOscPath() returns the correct path with variables" )
    {
        OscAudioParameter param { "/foo/{bar}" };
        REQUIRE ( param.getOscPath () == "/foo/{bar}" );
        REQUIRE ( param.getOscPath ( { { "{bar}", "baz" } } ) == "/foo/baz" );
    }

    SECTION ( "OscAudioParameter::getOscPath() returns the correct path with variables and leading slash" )
    {
        OscAudioParameter param { "foo/{bar}" };
        REQUIRE ( param.getOscPath () == "/foo/{bar}" );
        REQUIRE ( param.getOscPath ( { { "{bar}", "baz" } } ) == "/foo/baz" );
    }

    SECTION ( "OscAudioParameter::setOscPath() sets the correct path" )
    {
        OscAudioParameter param { "/foo" };
        REQUIRE ( param.getOscPath () == "/foo" );
        param.setOscPath ( "/bar" );
        REQUIRE ( param.getOscPath () == "/bar" );
    }
}

TEST_CASE ( "Basic ConcreteOscAudioParameter Tests", "[ConcreteOscAudioParameter]" )
{
    using namespace ts::mam;
    using namespace std::string_literals;

    SECTION ( "ConcreteOscAudioParameter::getOscPath() returns the correct path" )
    {
        ConcreteOscAudioParameter<juce::AudioParameterFloat> param { "/foo", "bar", "Bar", 0.f, 1.f, 0.5f };
        REQUIRE ( param.getOscPath () == "/foo" );
        REQUIRE ( param.getNormalisableRange ().start == 0.f );
        REQUIRE ( param.getNormalisableRange ().end == 1.f );
        REQUIRE ( param.get () == 0.5f );
    }

    SECTION ( "ConcreteOscAudioParameter::getOscPath() returns the correct path with leading slash" )
    {
        ConcreteOscAudioParameter<juce::AudioParameterFloat> param { "foo", "bar", "Bar", 0.f, 1.f, 0.5f };
        REQUIRE ( param.getOscPath () == "/foo" );
    }

    SECTION ( "ConcreteOscAudioParameter::getOscPath() returns the correct path with variables" )
    {
        ConcreteOscAudioParameter<juce::AudioParameterFloat> param { "/foo/{bar}", "bar", "Bar", 0.f, 1.f, 0.5f };
        REQUIRE ( param.getOscPath () == "/foo/{bar}" );
        REQUIRE ( param.getOscPath ( { { "{bar}", "baz" } } ) == "/foo/baz" );
    }

    SECTION ( "ConcreteOscAudioParameter::getOscPath() returns the correct path with variables and leading slash" )
    {
        ConcreteOscAudioParameter<juce::AudioParameterFloat> param { "foo/{bar}", "bar", "Bar", 0.f, 1.f, 0.5f };
        REQUIRE ( param.getOscPath () == "/foo/{bar}" );
        REQUIRE ( param.getOscPath ( { { "{bar}", "baz" } } ) == "/foo/baz" );
    }

    SECTION ( "ConcreteOscAudioParameter::setOscPath() sets the correct path" )
    {
        ConcreteOscAudioParameter<juce::AudioParameterFloat> param { "/foo", "bar", "Bar", 0.f, 1.f, 0.5f };
        REQUIRE ( param.getOscPath () == "/foo" );
        param.setOscPath ( "/bar" );
        REQUIRE ( param.getOscPath () == "/bar" );
    }
}

TEST_CASE ( "Basic makeOscAudioParam Tests", "[makeOscAudioParam]" )
{
    using namespace ts::mam;
    using namespace std::string_literals;

    SECTION ( "makeOscAudioParam() returns the correct path" )
    {
        auto param = makeOscAudioParam<juce::AudioParameterFloat> ( "/foo", "bar", "Bar", 0.f, 1.f, 0.5f );
        auto p     = dynamic_cast<ConcreteOscAudioParameter<juce::AudioParameterFloat>*> ( param.get () );
        REQUIRE ( p != nullptr );
        REQUIRE ( p->getOscPath () == "/foo" );
    }

    SECTION ( "makeOscAudioParam() returns the correct path with leading slash" )
    {
        auto param = makeOscAudioParam<juce::AudioParameterFloat> ( "foo", "bar", "Bar", 0.f, 1.f, 0.5f );
        auto p     = dynamic_cast<ConcreteOscAudioParameter<juce::AudioParameterFloat>*> ( param.get () );
        REQUIRE ( p != nullptr );
        REQUIRE ( p->getOscPath () == "/foo" );
    }

    SECTION ( "makeOscAudioParam() returns the correct path with variables" )
    {
        auto param = makeOscAudioParam<juce::AudioParameterFloat> ( "/foo/{bar}", "bar", "Bar", 0.f, 1.f, 0.5f );
        auto p     = dynamic_cast<ConcreteOscAudioParameter<juce::AudioParameterFloat>*> ( param.get () );
        REQUIRE ( p != nullptr );
        REQUIRE ( p->getOscPath () == "/foo/{bar}" );
        REQUIRE ( p->getOscPath ( { { "{bar}", "baz" } } ) == "/foo/baz" );
    }

    SECTION ( "makeOscAudioParam() returns the correct path with variables and leading slash" )
    {
        auto param = makeOscAudioParam<juce::AudioParameterFloat> ( "foo/{bar}", "bar", "Bar", 0.f, 1.f, 0.5f );
        auto p     = dynamic_cast<ConcreteOscAudioParameter<juce::AudioParameterFloat>*> ( param.get () );
        REQUIRE ( p != nullptr );
        REQUIRE ( p->getOscPath () == "/foo/{bar}" );
        REQUIRE ( p->getOscPath ( { { "{bar}", "baz" } } ) == "/foo/baz" );
    }

    SECTION ( "makeOscAudioParam() sets the correct path" )
    {
        auto param = makeOscAudioParam<juce::AudioParameterFloat> ( "/foo", "bar", "Bar", 0.f, 1.f, 0.5f );
        auto p     = dynamic_cast<ConcreteOscAudioParameter<juce::AudioParameterFloat>*> ( param.get () );
        REQUIRE ( p != nullptr );
        REQUIRE ( p->getOscPath () == "/foo" );
        p->setOscPath ( "/bar" );
        REQUIRE ( p->getOscPath () == "/bar" );
    }

    SECTION ( "makeFloatOscParam() works as expected" )
    {
        auto param = makeFloatOscParam ( "/foo", "bar", "Bar", 0.f, 1.f, 0.5f );
        REQUIRE ( param != nullptr );
        auto p = dynamic_cast<ConcreteOscAudioParameter<juce::AudioParameterFloat>*> ( param.get () );
        REQUIRE ( p != nullptr );
        REQUIRE ( p->getOscPath () == "/foo" );
        REQUIRE ( param->getNormalisableRange ().start == 0.f );
        REQUIRE ( param->getNormalisableRange ().end == 1.f );
        REQUIRE ( param->get () == 0.5f );
    }

    SECTION ( "makeBoolOscParam() works as expected" )
    {
        auto param = makeBoolOscParam ( "/foo", "bar", "Bar", true );
        REQUIRE ( param != nullptr );
        auto p = dynamic_cast<ConcreteOscAudioParameter<juce::AudioParameterBool>*> ( param.get () );
        REQUIRE ( p != nullptr );
        REQUIRE ( p->getOscPath () == "/foo" );
        REQUIRE ( param->get () == true );
    }

    SECTION ( "makeIntOscParam() works as expected" )
    {
        auto param = makeIntOscParam ( "/foo", "bar", "Bar", 0, 1, 0 );
        REQUIRE ( param != nullptr );
        auto p = dynamic_cast<ConcreteOscAudioParameter<juce::AudioParameterInt>*> ( param.get () );
        REQUIRE ( p != nullptr );
        REQUIRE ( p->getOscPath () == "/foo" );
        REQUIRE ( param->getNormalisableRange ().start == 0.f );
        REQUIRE ( param->getNormalisableRange ().end == 1.f );
        REQUIRE ( param->get () == 0 );
    }
}

TEST_CASE ( "makeRangeWithSkew() works as expected", "[makeRangeWithSkew]" )
{
    using namespace ts::mam;
    using namespace std::string_literals;

    SECTION ( "makeRangeWithSkew() works as expected" )
    {
        auto range = makeRangeWithSkew ( 0.f, 1.f, 0.5f );
        REQUIRE ( range.start == 0.f );
        REQUIRE ( range.end == 1.f );
        REQUIRE ( range.skew == 0.5f );
    }

    SECTION ( "makeRangeWithSkew() can be used with makeFloatOscParam()" )
    {
        auto param = makeFloatOscParam ( "/foo", "bar", "Bar", makeRangeWithSkew ( 0.f, 1.f, 0.5f ), 0.5f );
        REQUIRE ( param != nullptr );
        auto p = dynamic_cast<ConcreteOscAudioParameter<juce::AudioParameterFloat>*> ( param.get () );
        REQUIRE ( p != nullptr );
        REQUIRE ( p->getOscPath () == "/foo" );
        REQUIRE ( param->getNormalisableRange ().start == 0.f );
        REQUIRE ( param->getNormalisableRange ().end == 1.f );
        REQUIRE ( param->getNormalisableRange ().skew == 0.5f );
        REQUIRE ( param->get () == 0.5f );
    }
}

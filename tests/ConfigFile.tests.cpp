#include "ConfigFile.h"
#include <catch2/catch.hpp>
#include <juce_core/juce_core.h>

using juce::File;

class ConfigFileMock : public ts::mam::ConfigFile {
public:
    ConfigFileMock ()           = default;
    ~ConfigFileMock () override = default;

    [[nodiscard]] juce::File getConfigDirectory () const noexcept override
    {
        return File::getSpecialLocation ( File::tempDirectory )
                .getChildFile ( "de.2-shell.motu-avb-mixer" );
    }

    [[nodiscard]] File getFullPath() const noexcept
    {
        return getConfigDirectory ().getChildFile ( getConfigFileName () );
    }

    void cleanup () const noexcept
    {
        getConfigDirectory ().deleteRecursively ();
    }
};

class ConfigTest : public ts::mam::Config {
public:
    explicit ConfigTest ( const Config& other )
        : Config ( "" )
    {
        defaultDeviceConnection = other.getDefaultDeviceConnection ();
        defaultChannelCounts    = other.getDefaultChannelCounts ();
    }

    ~ConfigTest () override = default;

    DeviceConnection& getDefaultDeviceConnectionRef () noexcept
    {
        return defaultDeviceConnection;
    }

    ChannelCounts& getDefaultChannelCountsRef () noexcept
    {
        return defaultChannelCounts;
    }
};

TEST_CASE ( "ConfigFile Tests", "[ConfigFile]" )
{
    using namespace ts::mam;
    using juce::File;

    SECTION ( "Instantiate" )
    {
        REQUIRE_NOTHROW ( ConfigFile () );
    }

    SECTION ( "Parse non-existent file" )
    {
        ConfigFileMock cfgFile;
        cfgFile.cleanup ();
        auto cfg = cfgFile.load ();
        REQUIRE ( cfg.getDefaultDeviceConnection ().name == "default" );
        REQUIRE ( cfg.getDefaultDeviceConnection ().host == "127.0.0.1" );
        REQUIRE ( cfg.getDefaultDeviceConnection ().port == "9998" );
        REQUIRE ( cfg.getDefaultChannelCounts ().channel == 24 );
        REQUIRE ( cfg.getDefaultChannelCounts ().aux == 8 );
        REQUIRE ( cfg.getDefaultChannelCounts ().group == 8 );
        REQUIRE ( cfg.getDefaultChannelCounts ().reverb == 1 );
        REQUIRE ( cfg.getDefaultChannelCounts ().main == 1 );
        cfgFile.cleanup ();
    }

    SECTION ( "Parse empty JSON" )
    {
        ConfigFileMock cfgFile;
        REQUIRE (cfgFile.getFullPath().create().wasOk());
        REQUIRE (cfgFile.getFullPath().replaceWithText ( "" ));
        auto cfg = cfgFile.load ();
        REQUIRE ( cfg.getDefaultDeviceConnection ().name == "default" );
        REQUIRE ( cfg.getDefaultDeviceConnection ().host == "127.0.0.1" );
        REQUIRE ( cfg.getDefaultDeviceConnection ().port == "9998" );
        REQUIRE ( cfg.getDefaultChannelCounts ().channel == 24 );
        REQUIRE ( cfg.getDefaultChannelCounts ().aux == 8 );
        REQUIRE ( cfg.getDefaultChannelCounts ().group == 8 );
        REQUIRE ( cfg.getDefaultChannelCounts ().reverb == 1 );
        REQUIRE ( cfg.getDefaultChannelCounts ().main == 1 );
        cfgFile.cleanup ();
    }

    SECTION ( "Load configuration correctly" )
    {
        ConfigFileMock cfgFile;
        REQUIRE (cfgFile.getFullPath().create().wasOk());
        auto written = cfgFile.getFullPath().replaceWithText ( R"(
            {
                "connections": [
                    {
                        "name": "test",
                        "host": "foo",
                        "port": "bar"
                    }
                ],
                "channelCounts": {
                    "channel": 1,
                    "aux": 2,
                    "group": 3,
                    "reverb": 4,
                    "main": 5
                }
            }
        )" );
        REQUIRE (written);
        auto cfg = cfgFile.load ();
        REQUIRE ( cfg.getDefaultDeviceConnection ().name == "test" );
        REQUIRE ( cfg.getDefaultDeviceConnection ().host == "foo" );
        REQUIRE ( cfg.getDefaultDeviceConnection ().port == "bar" );
        REQUIRE ( cfg.getDefaultChannelCounts ().channel == 1 );
        REQUIRE ( cfg.getDefaultChannelCounts ().aux == 2 );
        REQUIRE ( cfg.getDefaultChannelCounts ().group == 3 );
        REQUIRE ( cfg.getDefaultChannelCounts ().reverb == 4 );
        REQUIRE ( cfg.getDefaultChannelCounts ().main == 5 );
        cfgFile.cleanup ();
    }

    SECTION ( "Save configuration correctly" )
    {
        ConfigFileMock cfgFile;
        auto defaultConfig = cfgFile.load ();
        ConfigTest modifiedConfig { defaultConfig };
        modifiedConfig.getDefaultDeviceConnectionRef ().name = "test";
        modifiedConfig.getDefaultDeviceConnectionRef ().host = "foo";
        modifiedConfig.getDefaultDeviceConnectionRef ().port = "bar";
        modifiedConfig.getDefaultChannelCountsRef ().channel = 1;
        modifiedConfig.getDefaultChannelCountsRef ().aux     = 2;
        modifiedConfig.getDefaultChannelCountsRef ().group   = 3;
        modifiedConfig.getDefaultChannelCountsRef ().reverb  = 4;
        modifiedConfig.getDefaultChannelCountsRef ().main    = 5;
        cfgFile.save ( modifiedConfig );
        auto reloadedCfg = cfgFile.load ();
        REQUIRE ( reloadedCfg.getDefaultDeviceConnection ().name == "test" );
        REQUIRE ( reloadedCfg.getDefaultDeviceConnection ().host == "foo" );
        REQUIRE ( reloadedCfg.getDefaultDeviceConnection ().port == "bar" );
        REQUIRE ( reloadedCfg.getDefaultChannelCounts ().channel == 1 );
        REQUIRE ( reloadedCfg.getDefaultChannelCounts ().aux == 2 );
        REQUIRE ( reloadedCfg.getDefaultChannelCounts ().group == 3 );
        REQUIRE ( reloadedCfg.getDefaultChannelCounts ().reverb == 4 );
        REQUIRE ( reloadedCfg.getDefaultChannelCounts ().main == 5 );
        cfgFile.cleanup ();
    }
}
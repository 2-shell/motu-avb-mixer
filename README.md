# Motu AVB Mixer Plugins

[![pipeline status](https://gitlab.com/2-shell/motu-avb-mixer/badges/main/pipeline.svg)](https://gitlab.com/2-shell/motu-avb-mixer/-/commits/main)
[![coverage report](https://gitlab.com/2-shell/motu-avb-mixer/badges/main/coverage.svg)](https://gitlab.com/2-shell/motu-avb-mixer/-/commits/main)

## What is this?

This repository hosts the source for a number of plugins for controlling the internal Mixer of MOTU AVB devices via OSC
from within a DAW.

The plugins are written in C++ _(C++17)_ and are based on the [JUCE](https://juce.com/) framework.

## Why would I want this?

MOTU AVB devices have a built-in mixer that is usually controlled via a web interface.

While that interface is quite powerful, it is not very convenient to use when you are working in a DAW, since you have
to constantly switch between the DAW and the browser, with the browser interface being (naturally) not as responsive and
as easy to control (e.g. no mousewheel support) as a native application.

Also, the mixer settings need to be saved and restored separately from the DAW project.

If you use a hardware controller, it is not easily possible to control the MOTU mixer and the DAW mixer at the same time.

Finally, the MOTU mixer does not support automation. So if you're e.g. using the MOTU mixer for summing, you cannot
automate the volume of the summed channels.

The plugins in this repo allow you to do all of the above.


## Which devices are supported?

The plugins are built against [MOTU's official OSC API, version 1.0.0](https://cdn-data.motu.com/downloads/audio/AVB/docs/OSC%20Quick%20Reference.pdf),
which AFAIK should be supported by all MOTU AVB devices.

Currently, the following devices have been tested:
- **MOTU LP32**

In terms of controllers, the plugins have been tested with the following devices:
- **Icon QCon Pro  / Icon QCon EX**

_Since that's the only device I currently have access to, I can obviously not test the plugins with other devices._  
_Any test reports on other MOTU AVB hardware or Controllers are highly appreciated!_

## Which DAWs are supported?

The plugins are built against the VST3, AU (macOS only) and LV2 (Linux only) formats, which should be supported by most DAWs.

Currently, the following DAWs have been tested:
- **Cubase 11 / 12**
- **Reaper 6 / 7**
- **Logic Pro X**
- **Ableton Live 11**

_Since I don't have access to other DAWs, I cannot test the plugins with other DAWs._
_Any test reports on different DAWs are highly appreciated!_

## Which OSs are supported?

The plugins are built against the following OSs:
- **macOS (x86_64, arm64)**
- **Windows 10 (x86_64)**
- **Linux (x86_64)**

## Which features are supported?

The full feature set is distributed across multiple plugins, serving different purposes:
- **MOTU AVB Mixer - Channel Strip**   
  - This is the centerpiece of the plugin suite.  
    It allows controlling all aspects of a single channel of the MOTU mixer, including:  
    high-pass, gate, compressor, EQ, sends, pan,mute, solo and the channel fader.
  - The idea is to load an instance of this plugin on each input channel or the corresponding track in your DAW and use
    it to control the MOTU mixer channel that is connected to that input.
  - Which channel is controlled by the plugin is determined by the plugin's channel number parameter.
  - All settings are saved and restored with the DAW project. When the DAW project is loaded, the plugin will
    automatically connect to the corresponding MOTU mixer channel and restore the stored settings.
  - This plugin also supports automation of all parameters.
- **MOTU AVB Mixer - Summing**
  - This plugin provides a simple interface for controlling all main mix faders of the MOTU mixer.
  - The idea is to load an instance of this plugin on the master channel of your DAW and use it to control the MOTU
    mixer's main mix faders.
  - For example, when using a controller like the QCon Pro, you can use the faders of the controller to control the
    MOTU mixer's main mix faders, by selecting the MOTU AVB Mixer Summing plugin and using the _Flip_ button on the
    controller.
- **MOTU AVB Mixer - Aux Mix**
  - Similar to the MOTU AVB Mixer Summing plugin, this plugin provides a simple interface for controlling all aux mix
    faders of the MOTU mixer.
  - This could probably be merged into the MOTU AVB Mixer Summing plugin, but I decided to keep it separate for now.

There are a number of features, that the OSC API provides, but that are not yet supported by the plugins.  
I have no plans in the immediate future to implement those, since the plugins are currently catered to my personal
needs.  
But if you are missing any features, feel free to open an issue or a merge request.

## Status

The plugins are currently in an early stage of development.

Currently there is no GUI, so all parameters are controlled via the DAW's generic parameter interface.  
Also, settings like IP-Address and Destination Port need to be configured via configuration files.

Besides that, the plugins are fully functional.  
Use in a production environment is not recommended yet, though.

**Generally, the contents of this repo are provided as-is, without any warranty.  
Use at your own risk.**

## How do I build the plugins? (WIP)

### Prerequisites

- Windows / macOS
- **CMake >= 3.19**
- **C++ compiler supporting C++17 and a working C++ build environment**

### Building

1. Clone this repo
2. Run `cmake -B build -S .` in the repo root
3. Run `cmake --build build` in the repo root

## How do I install the plugins?

### macOS

1. Copy the built plugin to `/Users/yourusername/Library/Audio/Plug-Ins/VST3` (for VST3) or `/Users/yourusername/Library/Audio/Plug-Ins/Components` (for AU)
2. Restart your DAW

### Windows

1. Copy the built plugin to `C:\Program Files\Common Files\VST3` (for VST3) or `C:\Program Files\Common Files\Avid\Audio\Plug-Ins` (for AAX)
2. Restart your DAW